<?php
namespace App\GraphQL\Mutations;

use Closure;
use App\User;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Mutation;

class UpdateUserPasswordMutation extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateUserPassword'
    ];

    public function type(): Type
    {
        return GraphQL::type('user');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
            'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = User::find($args['id']);
        if(!$user) {
            return null;
        }

        $user->name = $args['name'];
        $user->save();

        return $user;
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required'],
            'name' => ['required'],
        ];
    }

    protected function messages(array $args = []): array
    {
        return [
            'id' => 'ID không được để trống',
            'name' => 'Tên không được để trống',
        ];
    }
}
